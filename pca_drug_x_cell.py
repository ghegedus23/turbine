import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from plotly import graph_objects as go
from plotly import express as px
import os

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fPath_tidydata = os.path.join(os.getcwd(), 'data', 'tidydata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]
fname = "pca_test.csv"
fPath = os.path.join(fPath_tidydata, fname)

df = pd.read_csv(fPath, sep=';', decimal=',')

# scale data
scaler = StandardScaler()
df_scaled = df.copy()
df_scaled.drop(columns=["compound name"], inplace=True)
df_scaled = pd.DataFrame(scaler.fit_transform(df_scaled), columns=df_scaled.columns)

# perform PCA
pca = PCA(n_components=2)
pca_fit = pca.fit_transform(df_scaled)

# Scree plot
pc_vals = np.arange(pca.n_components_) + 1
fig = px.bar(x=pc_vals, y=pca.explained_variance_ratio_)
fig.show()

# PCA plot
df_pca = pd.DataFrame(data=pca_fit, columns=['PC1', 'PC2'])
fig = px.scatter(x=df_pca['PC1'], y=df_pca['PC2'], text=df['compound name'])
fig.show()

print("ready")