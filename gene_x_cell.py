import pandas as pd
import numpy as np
from requests.api import get
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from plotly import graph_objects as go
from plotly import express as px
import os
import sys
import csv
import requests

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

def get_display_name(gene_id):
    url = "https://rest.ensembl.org/lookup/id/" + gene_id + "?content-type=application/json"
    r = requests.get(url)
    return r.json()['display_name']

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fPath_tidydata = os.path.join(os.getcwd(), 'data', 'tidydata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]

# read treatment data
fname = "pca_test.csv"
fPath = os.path.join(fPath_tidydata, fname)
df_td = pd.read_csv(fPath, sep=';', decimal=',')

cols = df_td.columns
col_names = dict()
for col in cols:
    col_names[col] = col.upper().strip()
df_td.rename(columns=col_names, inplace=True)

# read genes data
fname = "CCLE_RNAseq_rsem_genes_tpm_20180929.txt"
fPath = os.path.join(fPath_rawdata, fname)
df_gd = pd.read_csv(fPath, sep='\t', nrows=10)

# print(df_gd.shape)

cols = df_gd.columns
genes = df_gd['gene_id']
df_gd['gene_name'] = df_gd.apply(lambda x: get_display_name(x['gene_id'][:x['gene_id'].find('.')]), axis=1)

a = np.array([])
mrna = np.array(df_gd['transcript_ids'])
i = 0
i_max = len(mrna)

s = 0
for x in mrna:
    s += len(x)
    i += 1
    if i%50==0:
        status = "\rProgress: " + "{:.2%}".format(i/i_max)
        sys.stdout.write(status)

print('\n')
print(s)

col_names = dict()
for i in range(2, len(cols)):
    old_name = cols[i]
    new_name = cols[i][:cols[i].find("_")]
    col_names[old_name] = new_name.upper().strip()

df_gd.rename(columns=col_names, inplace=True)
df_gd.drop(columns=df_gd.columns.difference(df_td.columns), inplace=True)

cell_lines = df_gd.columns

# plot two genes
fig = px.scatter(x=np.array(df_gd.iloc[0]), y=np.array(df_gd.iloc[1]), color=cell_lines, size=np.array(df_gd.iloc[3]))
fig.show()

print("ready")