import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from plotly import graph_objects as go
from plotly import express as px
import os
import csv

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fPath_tidydata = os.path.join(os.getcwd(), 'data', 'tidydata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]


# read treatment data
fname = "pca_test.csv"
fPath = os.path.join(fPath_tidydata, fname)
df_td = pd.read_csv(fPath, sep=';', decimal=',')

cols = df_td.columns
col_names = dict()
for col in cols:
    col_names[col] = col.upper().strip()
df_td.rename(columns=col_names, inplace=True)

# read genes data
fname = "CCLE_RNAseq_rsem_genes_tpm_20180929.txt"
fPath = os.path.join(fPath_rawdata, fname)
# with open(fPath) as f:
#     print(f.readline())
df_gd = pd.read_csv(fPath, sep='\t', nrows=10)
cols = df_gd.columns
genes = df_gd['gene_id']

col_names = dict()
for i in range(2, len(cols)):
    old_name = cols[i]
    new_name = cols[i][:cols[i].find("_")]
    col_names[old_name] = new_name.upper().strip()

# with open(os.path.join(fPath_tidydata, 'cell_lines.csv'), 'w', newline='') as f:
#     writer = csv.writer(f, delimiter=',')
#     for key, val in col_names.items():
#         writer.writerow([val])

df_gd.rename(columns=col_names, inplace=True)
df_gd.drop(columns=df_gd.columns.difference(df_td.columns), inplace=True)

cell_lines = df_gd.columns

print(len(df_gd.columns))
# scale data
scaler = StandardScaler()
df_scaled = df_gd.T.copy()
df_scaled = pd.DataFrame(scaler.fit_transform(df_scaled), columns=df_scaled.columns)

# perform PCA
pca = PCA(n_components=2)
pca_fit = pca.fit_transform(df_scaled)

# Scree plot
pc_vals = np.arange(pca.n_components_) + 1
fig = px.bar(x=pc_vals, y=pca.explained_variance_ratio_)
fig.show()

# PCA plot
df_pca = pd.DataFrame(data=pca_fit, columns=['PC1', 'PC2'])
fig = px.scatter(x=df_pca['PC1'], y=df_pca['PC2'], color=cell_lines)
fig.show()

print("ready")