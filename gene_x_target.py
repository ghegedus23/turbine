import pandas as pd
import numpy as np
from requests.api import get
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from plotly import graph_objects as go
from plotly import express as px
import os
import sys
import csv
import requests
import urllib.parse
import urllib.request

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

def get_display_name(gene_id):
    url = "https://rest.ensembl.org/lookup/id/" + gene_id + "?content-type=application/json"
    r = requests.get(url)
    return r.json()['display_name']

def get_gene_id(symbol):
    url = "https://rest.ensembl.org/lookup/symbol/homo_sapiens/" + symbol
    r = requests.get(url)
    txt = r.text
    if "error" in txt:
        return ["", ""]
    else:
        gene_id = txt[txt.find("\nid: ")+len("\nid: "):]
        gene_id = gene_id[:gene_id.find("\n")]
        version = txt[txt.find("\nversion")+len("\nversion: "):]
        version = version[:version.find("\n")]
        return gene_id, version

def query_UP(_from, _to, _query):
        params = {
            'from': _from,
            'to': _to,
            'format': 'tab',
            'query': '\t'.join(_query),
            'taxon': '9606'
        }
        data = urllib.parse.urlencode(params)
        data = data.encode('utf-8')
        url = 'https://www.uniprot.org/uploadlists/'
        req = urllib.request.Request(url, data)
        with urllib.request.urlopen(req) as f:
            response = f.read()
        return response.decode('utf-8')        

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fPath_tidydata = os.path.join(os.getcwd(), 'data', 'tidydata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]

# read treatment data
fname = "pca_test.csv"
fPath = os.path.join(fPath_tidydata, fname)
df_td = pd.read_csv(fPath, sep=';', decimal=',')

cols = df_td.columns
col_names = dict()
for col in cols:
    col_names[col] = col.upper().strip()
df_td.rename(columns=col_names, inplace=True)

# read profiling data
fname = "CCLE_NP24.2009_profiling_2012.02.20.csv"
fPath = os.path.join(fPath_rawdata, fname)
df_pd = pd.read_csv(fPath, sep=',', encoding='ANSI')

df_targets = df_pd['Target(s)']

# read possible target genes
fname = "targets_genes.xlsx"
fPath = os.path.join(fPath_tidydata, fname)
df_tg = pd.read_excel(fPath)
tg_gene_ids = df_tg['Ensemble']

# read genes data
fname = "CCLE_RNAseq_rsem_genes_tpm_20180929.txt"
fPath = os.path.join(fPath_rawdata, fname)
df_gd = pd.read_csv(fPath, sep='\t')

cols = df_gd.columns
df_gd['gene_id'] = df_gd.apply(lambda x: x['gene_id'][:x['gene_id'].find('.')], axis=1)
genes = df_gd['gene_id']

df = pd.merge(df_tg, df_gd, how='inner', left_on='Ensemble', right_on='gene_id')
df.to_csv(os.path.join(fPath_tidydata, 'RNAseq_tpm_targets.csv'))
# df_gd['gene_name'] = df_gd.apply(lambda x: get_display_name(x['gene_id'][:x['gene_id'].find('.')]), axis=1)

# a = np.array([])
# mrna = np.array(df_gd['transcript_ids'])
# i = 0
# i_max = len(mrna)
# s = 0
# for x in mrna:
#     s += len(x)
#     i += 1
#     if i%50==0:
#         status = "\rProgress: " + "{:.2%}".format(i/i_max)
#         sys.stdout.write(status)
# print('\n')
# print(s)

col_names = dict()
for i in range(2, len(cols)):
    old_name = cols[i]
    new_name = cols[i][:cols[i].find("_")]
    col_names[old_name] = new_name.upper().strip()

df_gd.rename(columns=col_names, inplace=True)
df_gd.drop(columns=df_gd.columns.difference(df_td.columns), inplace=True)

cell_lines = df_gd.columns

# plot two genes
fig = px.scatter(x=np.array(df_gd.iloc[0]), y=np.array(df_gd.iloc[1]), color=cell_lines, size=np.array(df_gd.iloc[3]))
fig.show()

print("ready")