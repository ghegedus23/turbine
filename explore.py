import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly import express as px
import os

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]
fname = "CCLE_NP24.2009_Drug_data_2015.02.24.csv"
fPath = os.path.join(fPath_rawdata, fname)

# with open(fPath) as f:
#     print(f.readline())

df_dd = pd.read_csv(fPath, sep=',', decimal='.')

compounds = np.unique(df_dd['Compound'])
targets = np.unique(df_dd['Target'])
print("Number of compounds: "+str(len(compounds)))
print("Number of targets: "+str(len(targets)))

idx = 'Primary Cell Line Name'
clm = 'Target'
val = 'IC50 (uM)'

# table = pd.pivot_table(df_dd, index=idx, columns=clm, values=val, aggfunc=np.mean)
# fig = px.imshow(table)
# fig = go.Figure(data=go.Heatmap(df_to_plotly(table)))
fig = px.scatter(df_dd, x="IC50 (uM)", y="EC50 (uM)", color="FitType")
fig.show()

print("Ready!")