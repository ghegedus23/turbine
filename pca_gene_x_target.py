import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from plotly import graph_objects as go
from plotly import express as px
import os
import csv

def df_to_plotly(df):
    return {'z': df.values.tolist(),
            'x': df.columns.tolist(),
            'y': df.index.tolist()}

fPath_rawdata = os.path.join(os.getcwd(), 'data', 'rawdata')
fPath_tidydata = os.path.join(os.getcwd(), 'data', 'tidydata')
fnames = [
    "CCLE_GNF_data_090613.xls", 
    "CCLE_NP24.2009_Drug_data_2015.02.24.csv", 
    "CCLE_NP24.2009_profiling_2012.02.20.csv", 
    "CCLE_RNAseq_rsem_genes_tpm_20180929.txt", 
    "Cell_lines_annotations_20181226.txt"
    ]


# read treatment data
fname = "pca_test.csv"
fPath = os.path.join(fPath_tidydata, fname)
df_td = pd.read_csv(fPath, sep=';', decimal=',')

cols = df_td.columns
col_names = dict()
for col in cols:
    col_names[col] = col.upper().strip()
df_td.rename(columns=col_names, inplace=True)

# read possible target genes
fname = "RNAseq_tpm_targets.csv"
fPath = os.path.join(fPath_tidydata, fname)
df_tg = pd.read_csv(fPath, sep=',', decimal='.')

genes = df_tg['gene_id']
targets = df_tg['Target']

# map cell lines to match the ones in treatment data
cols = df_tg.columns
col_names = dict()
for i in range(7, len(cols)):
    old_name = cols[i]
    new_name = cols[i][:cols[i].find("_")]
    col_names[old_name] = new_name.upper().strip()
df_tg.rename(columns=col_names, inplace=True)

# filter cell lines
df_tg.drop(columns=df_tg.columns.difference(df_td.columns), inplace=True)
cell_lines = df_tg.columns

# write data
df_tg.to_csv(os.path.join(fPath_tidydata, 'pca_genes_cells.csv'))

print(len(df_tg.columns))
# scale data
scaler = StandardScaler()
df_scaled = df_tg.copy()
df_scaled = pd.DataFrame(scaler.fit_transform(df_scaled), columns=df_scaled.columns)

# perform PCA
pca = PCA(n_components=2)
pca_fit = pca.fit_transform(df_scaled)

# Scree plot
pc_vals = np.arange(pca.n_components_) + 1
fig = px.bar(x=pc_vals, y=pca.explained_variance_ratio_)
fig.show()

# PCA plot
df_pca = pd.DataFrame(data=pca_fit, columns=['PC1', 'PC2'])
# fig = px.scatter(x=df_pca['PC1'], y=df_pca['PC2'], color=cell_lines)
fig = px.scatter(x=df_pca['PC1'], y=df_pca['PC2'])
fig.show()

print("ready")